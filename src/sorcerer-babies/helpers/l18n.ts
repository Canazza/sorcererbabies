export class l18n {
    public static instance:l18n;
    cache: Phaser.Cache.CacheManager;
    public static get(key:string):string {
         return this.instance.get(key);
    }
    constructor(cache:Phaser.Cache.CacheManager) {
        this.cache = cache;
    }
    public get(key:string):string {
        
        return this.cache.json.get("strings")[key] || key;
        
    }
}