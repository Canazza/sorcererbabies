export class MathHelper {
    public static getDistance2(x1:number ,y1:number ,x2:number ,y2:number ):number  {
        return ((x1-x2) * (x1-x2)) + ((y1-y2) * (y1-y2));
    }
    public static getDistance(x1:number ,y1:number ,x2:number ,y2:number ):number {
        return Math.sqrt(MathHelper.getDistance2(x1 ,y1 ,x2 ,y2 ));
    }
    public static lerp(min:number,max:number,t:number):number {

        return (max - min) * t + min;
    } 
    public static clamp(value,min,max):number {
        return Math.min(Math.max(value,min),max);
    }
    private static rollRegEx:RegExp = /(\d*)d(\d+)([-+]\d+)?/ig;
    public static roll(r:string, ignoreNegatives:boolean = true):number {
        let total:number = 0;
        var match:RegExpExecArray;
        while( match = this.rollRegEx.exec(r)) { 
            let rollTotal = 0;
            let count = parseInt(match[1]) || 1;
            let dice = parseInt(match[2]);
            let add = parseInt(match[3]) || 0; 
            for(let i = 0; i < count; i++) {
                rollTotal += (Math.floor(Math.random() * dice) + 1);  
            }
            rollTotal += add || 0;
            if(!ignoreNegatives || rollTotal > 0)
                total += rollTotal;
        } 
        return total;
    }
}