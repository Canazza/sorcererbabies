
import { AnimationHelper } from "../helpers/animation-helper";  
import { MessageBox, MessageBoxConfig, MesssageBoxConfigFactory } from "../objects/messagebox";
import { Button } from "../objects/button";
import { Intro } from "../objects/intro";
import { l18n } from "../helpers/l18n";
import { BattleController, BattleFactory, BattleEvent, BattleConfig } from "../objects/battlecontroller";
import { GameController } from "../objects/gamecontroller";
import { BattleEventDisplay } from "../objects/battleeventdisplay";


export class GameScene extends Phaser.Scene {   
    private map:Phaser.Tilemaps.Tilemap;
    private groundLayer:Phaser.Tilemaps.StaticTilemapLayer;
    private elementsLayer:Phaser.Tilemaps.StaticTilemapLayer;
    private cameraFocus:Phaser.GameObjects.Graphics;
    private WELL_INDEX:integer = 13;
    private popup:MessageBox;
    private intro:Intro;
    private gameController:GameController;
    private battleController:BattleController;
    constructor() {
      super({
        key: "GameScene"
      });
    }
  
    init(): void {   
        /*window.addEventListener("resize", () => {
            this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        })*/
        
        this.map = this.make.tilemap({key:'town-map'}); 
        this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels); 
        var townmap = this.map.addTilesetImage('TownMap', 'TownMap');          
        var wellmap = this.map.addTilesetImage("Well","Well"); 
        
        this.groundLayer = this.map.createStaticLayer("Ground",townmap,0,0);
        this.groundLayer.setPosition(( this.cameras.main.width-this.groundLayer.width +16)/2,(this.cameras.main.height-this.groundLayer.height +16)/2);
        this.elementsLayer = this.map.createStaticLayer("Elements",townmap,0,0);
        this.elementsLayer.setPosition(this.groundLayer.x,this.groundLayer.y);
         
        
        //this.cameras.main.startFollow(this.cameraFocus); 
        this.intro = new  Intro(this);
      this.gameController = new GameController();
      this.battleController = new BattleController(this);
      this.battleEventDisplayer = new BattleEventDisplay(this);
      this.battleEventDisplayer.addListener("completed",() => {        
        this.battleEventDisplayer.setVisible(false);
        this.currentBattleEvent = null
        if(this.battleEvents.length == 0 ) {
          console.log(this.currentDay ,">=", this.endOfDayBattles.length )
          if(!this.tutorialCompleted) {
            this.TutorialBattleCompleted();
          } else {
            if(this.gameController.town.babyCount <= 0) {
              this.scene.start("FailScene");
            } else 
            if(this.currentDay >= this.endOfDayBattles.length) {                
              console.log("END OF GAME");
              this.scene.start("WinScene")
            } else {
              this.showDayMenu();
            } 
          }
        } else {
        }
      }); 
      this.battleEventDisplayer.width = 250;
      this.battleEventDisplayer.height = 150;
      this.battleEventDisplayer.centerOn(this.cameras.main.width/2,this.cameras.main.height/2)
      this.battleEventDisplayer.x += this.cameras.main.scrollX;
      this.battleEventDisplayer.y += this.cameras.main.scrollY;
      this.add.existing(this.battleEventDisplayer);
      this.battleEventDisplayer.setVisible(false);
        //this.popup = new MessageBox(this,0,0,MessageBoxConfig.OK_CANCEL("","One day, a well appeared in the middle of the village",() => {console.log("OKAY PRESSED");},() => { console.log("CANCEL PRESSED")}));
        //this.popup.centerOn(this.cameras.main.width/2,this.cameras.main.height/2);
        
    } 
    public battleEventDisplayer:BattleEventDisplay;
    public battleEvents:BattleEvent[];
    public currentBattle:BattleConfig;
    create(): void {  
      console.log("GAME SCENE CREATE");
      this.currentDay = 0;
      l18n.instance = new l18n(this.cache);   
      this.intro.setConfig(this.intro.intro1);
      this.intro.once(Intro.COMPLETED_EVENT, () =>{ 
          console.log("Intro 1 Completed") 
          this.StartTutorialBattle();       
      });
      //this.StartBattle(this.battles[7]);
    } 
    private tutorialCompleted:boolean = false;
    StartTutorialBattle() {
      this.currentBattle = new BattleFactory("Goblin Scouting Party")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)
      .addAttack("Goblin Scout Shortbow").setRoll("d20+4","1d6+2").setTargets(1) 
      .Build();
      this.battleEvents = this.battleController.DoBattle(this.currentBattle,this.gameController.town); 
      //this.battleEventDisplayer.once("completed", this.TutorialBattleCompleted);
    }
    TutorialBattleCompleted() { 
      this.tutorialCompleted = true;
      this.intro.setConfig(this.intro.intro2);
      this.intro.once(Intro.COMPLETED_EVENT, () =>{ 
          console.log("Intro 2 Completed")  
          this.showDayMenu();
          //this.EndOfDay();
          //this.StartBattle(this.battles[4]());
      });
    }
    StartBattle(battleConfig:BattleConfig) {
      this.currentBattle = battleConfig;
      this.battleEvents = this.battleController.DoBattle(this.currentBattle,this.gameController.town); 
    }
    public currentBattleEvent:BattleEvent;
    update(timestamp,delta):void {
        if(!this.currentBattleEvent && this.battleEvents && this.battleEvents.length > 0){
          this.currentBattleEvent = this.battleEvents.shift();
          this.battleEventDisplayer.displayEvent(this.currentBattle,this.currentBattleEvent);
          this.battleEventDisplayer.setVisible(true);
        } else if(this.currentBattleEvent) {
          this.battleEventDisplayer.update();
        }
    }
    public currentDay = 0;
    public dayMenu:MessageBox;
    public showDayMenu() {
      this.dayMenu = this.dayMenu || new MessageBox(this,0,0);
      let townStatus = [];
      townStatus.push(this.gameController.town.babyCount+"/"+this.gameController.town.townCapacity +" Sorcerer Babies");
      townStatus.push(this.gameController.freePopulation +" unassigned population");
      townStatus.push(this.gameController.gold + " resources");
      townStatus.push(this.gameController.builders + " building Creches");
      townStatus.push(this.gameController.gatherers + " gathering resources");
      townStatus.push(this.gameController.recruitment + " recruiting refugees");
      let config = new MesssageBoxConfigFactory().setTitle("Work Menu - Day " + (this.currentDay + 1)).setMessage(townStatus.join("\n")).setWidth(300);
      config.addButton("Recruit People",() => {this.recruitPeople()})
      config.addButton("Build Creche",() => {this.buildCreche()})
      config.addButton("Gather Resources",() => {this.gatherResources()})
      config.addButton("Reset",() => {this.ResetDay()});
      config.addButton("Next Day",() => {this.EndOfDay()});
 
      this.dayMenu.config = config.Build();
      this.dayMenu.centerOn(this.cameras.main.width/2,this.cameras.main.height/2);
    }
    public ResetDay() {
      this.gameController.ResetDay();
      this.showDayMenu();
    }
    public gatherResources() {
      this.dayMenu = this.dayMenu || new MessageBox(this,0,0);
      let townStatus = [];
      townStatus.push(this.gameController.freePopulation +" unassigned population");
      townStatus.push(this.gameController.gatherers + " gathering resources");
      townStatus.push(this.gameController.gold + " resources");
      let config = new MesssageBoxConfigFactory().setTitle("Recruitment Menu").setMessage(townStatus.join("\n")).setWidth(300);
      if(this.gameController.canGatherResources(1)) {
        config.addButton("Assign 2 to Gather (2d4)R",() => {
          this.gameController.gatherResources(1);
          this.gatherResources()
        })
      } 
      if(this.gameController.canGatherResources(5)) {
        config.addButton("Assign 10 to Gather (10d4)R",() => {
          this.gameController.gatherResources(5);
          this.gatherResources()
        })
      } 
      if(this.gameController.canGatherResources(50)) {
        config.addButton("Assign 100 to Gather (100d4)R",() => {
          this.gameController.gatherResources(50);
          this.gatherResources()
        })
      } 
      config.addButton("Back",() => {this.showDayMenu()}); 
      this.dayMenu.config = config.Build();
      this.dayMenu.centerOn(this.cameras.main.width/2,this.cameras.main.height/2);
    }
    public buildCreche() {
      this.dayMenu = this.dayMenu || new MessageBox(this,0,0);
      let townStatus = [];
      townStatus.push(this.gameController.freePopulation +" unassigned population");
      townStatus.push(this.gameController.builders + " building creches");
      townStatus.push(this.gameController.gold + " resources");
      let config = new MesssageBoxConfigFactory().setTitle("Creche Menu").setMessage(townStatus.join("\n")).setWidth(300);
      if(this.gameController.canBuildCreche(1)) {
        config.addButton("Assign 2 to build a Creche (10R)",() => {
          this.gameController.buildCreche(1);
          this.buildCreche()
        })
      }  
      if(this.gameController.canBuildCreche(5)) {
        config.addButton("Assign 10 to build Creches (50R)",() => {
          this.gameController.buildCreche(5);
          this.buildCreche()
        })
      }  
      if(this.gameController.canBuildCreche(50)) {
        config.addButton("Assign 100 to build Creches (500R)",() => {
          this.gameController.buildCreche(50);
          this.buildCreche()
        })
      }  
      config.addButton("Back",() => {this.showDayMenu()}); 
      this.dayMenu.config = config.Build();
      this.dayMenu.centerOn(this.cameras.main.width/2,this.cameras.main.height/2);
    }
    public recruitPeople() {
      this.dayMenu = this.dayMenu || new MessageBox(this,0,0);
      let townStatus = [];
      townStatus.push(this.gameController.freePopulation +" unassigned population");
      townStatus.push(this.gameController.recruitment + " recruiting people"); 
      let config = new MesssageBoxConfigFactory().setTitle("Recruit Menu").setMessage(townStatus.join("\n")).setWidth(300);
      if(this.gameController.canRecruit(1)) {
        config.addButton("Assign 2 to recruit",() => {
          this.gameController.recruitPeople(1);
          this.recruitPeople()
        })
      }  
      if(this.gameController.canRecruit(5)) {
        config.addButton("Assign 10 to recruit",() => {
          this.gameController.recruitPeople(5);
          this.recruitPeople()
        })
      }  
      if(this.gameController.canRecruit(50)) {
        config.addButton("Assign 100 to recruit",() => {
          this.gameController.recruitPeople(50);
          this.recruitPeople()
        })
      }  
      config.addButton("Back",() => {this.showDayMenu()}); 
      this.dayMenu.config = config.Build();
      this.dayMenu.centerOn(this.cameras.main.width/2,this.cameras.main.height/2);

    } 
    public EndOfDay() {
      let townStatus = this.gameController.SpawnTick();
      if(this.currentDay < this.endOfDayBattles.length) { 
      this.dayMenu = this.dayMenu || new MessageBox(this,0,0);  
      let newDayConfig = new MesssageBoxConfigFactory().setTitle("Day " + (this.currentDay + 1) + " ended").setMessage(townStatus.join("\n")).setWidth(300);
       
      newDayConfig.addButton("Continue",() => {           
          let battleIndex = this.endOfDayBattles[this.currentDay];          
          let config = this.battles[battleIndex]();
          this.StartBattle(config);
          this.currentDay ++;          
        }) 
        this.dayMenu.config = newDayConfig.Build();
      }
    }
    public endOfDayBattles:integer[] = [
      0,0,0,0,1,
      0,0,0,0,2,
      1,1,1,1,2,
      2,2,2,2,3,
      2,2,2,2,4,
      3,3,3,3,4,
      3,4,3,4,5,
      3,4,3,4,6,
      3,4,3,4,7
      
    ]
    public battles:Function[] = [
      () =>
      new BattleFactory("Goblin Raid")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #3","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #4","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .Build(),

      () =>
      new BattleFactory("Goblin Raid")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #3","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #4","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Hobgoblin","hobgoblin").setHealth(11)
      .addAttack("Hobgoblin Longsword (Cleave)").setRoll("d20+3","1d10+1").setTargets(3)
      .Build(),

      () =>
      new BattleFactory("Goblin Strike Team")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Hobgoblin #1","hobgoblin").setHealth(11)
      .addAttack("Hobgoblin Longsword (Cleave)").setRoll("d20+3","1d10+1").setTargets(3)
      .addEnemy("Hobgoblin #2","hobgoblin").setHealth(11)
      .addAttack("Hobgoblin Longsword (Cleave)").setRoll("d20+3","1d10+1").setTargets(3)
      .addEnemy("Hobgoblin #3","hobgoblin").setHealth(11)
      .addAttack("Hobgoblin Longsword (Cleave)").setRoll("d20+3","1d10+1").setTargets(3)
      .Build(),

      () =>
      new BattleFactory("Bugbear Crew")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Hobgoblin","hobgoblin").setHealth(11)
      .addAttack("Hobgoblin Longsword (Cleave)").setRoll("d20+3","1d10+1").setTargets(3)
      .addEnemy("Bugbear #1","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear #2","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5).Build(),

      () =>
      new BattleFactory("Manticore")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Manticore","green-dragon").setHealth(68)      
      .addAttack("Bite").setRoll("d20+5","1d8+3").setTargets(1) 
      .addAttack("Claw").setRoll("d20+5","1d6+3").setTargets(1) 
      .addAttack("Tail Spike").setRoll("d20+5","1d8+3").setTargets(1)
      .Build(),

      () =>
      new BattleFactory("Chimera")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Chimera","green-dragon").setHealth(114) 
      .addAttack("Breath").setRoll("d20+10","7d8").setAoE(true).setTargets(7).setRecharge(5).setEndIfUsed(true)     
      .addAttack("Bite").setRoll("d20+7","2d6+4").setTargets(1) 
      .addAttack("Horns").setRoll("d20+7","1d12+4").setTargets(1) 
      .addAttack("Claws").setRoll("d20+7","2d6+4").setTargets(1) 
      .Build() ,

      () =>
      new BattleFactory("Young Green Dragon")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Vertigar","green-dragon").setHealth(136) 
      .addAttack("Breath").setRoll("d20+10","12d6").setAoE(true).setTargets(42).setRecharge(5).setEndIfUsed(true)     
      .addAttack("Bite").setRoll("d20+7","2d10+4, 2d6").setTargets(1) 
      .addAttack("Claw").setRoll("d20+7","2d6+4").setTargets(1) 
      .Build() ,

      () =>
      new BattleFactory("2 Young Green Dragons")
      .addEnemy("Goblin #1","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1)
      .addEnemy("Goblin #2","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Goblin #3","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Goblin #4","goblin").setHealth(7)      
      .addAttack("Goblin Scimitar").setRoll("d20+4","1d6+2").setTargets(1) 
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Bugbear","bugbear").setHealth(27)
      .addAttack("Bugbear Morningstar (Cleave)").setRoll("d20+4","2d8+2").setTargets(5)
      .addEnemy("Gixifer","green-dragon").setHealth(136) 
      .addAttack("Breath").setRoll("d20+10","12d6").setAoE(true).setTargets(42).setRecharge(5).setEndIfUsed(true)     
      .addAttack("Bite").setRoll("d20+7","2d10+4, 2d6").setTargets(1) 
      .addAttack("Claw").setRoll("d20+7","2d6+4").setTargets(1) 
      .addEnemy("Oxylonirfask","green-dragon").setHealth(136) 
      .addAttack("Breath").setRoll("d20+10","12d6").setAoE(true).setTargets(42).setRecharge(5).setEndIfUsed(true)     
      .addAttack("Bite").setRoll("d20+7","2d10+4, 2d6").setTargets(1) 
      .addAttack("Claw").setRoll("d20+7","2d6+4").setTargets(1) 
      .Build() 




    ]
  }
  