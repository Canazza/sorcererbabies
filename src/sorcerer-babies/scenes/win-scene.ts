 

export class WinScene extends Phaser.Scene { 

  constructor() {
    super({
      key: "WinScene"
    });
  }

  init(): void { 
    // set the background and create loading bar
    this.cameras.main.setBackgroundColor(0x98d687); 
    let text = this.add.text(6,2,"Congratulations!\nThe Goblin Army is dead!\nEnjoy raising all those sorcerer babies!")
    text.setOrigin(0.5,0.5);
    text.setPosition(this.cameras.main.width/2,this.cameras.main.height/2);
    text.setAlign("center");
    text.setWordWrapWidth(this.cameras.main.width-6); 
    text.addListener("pointerup",() => {
        window.location.reload();
    });
    text.setInteractive();
  } 
}
