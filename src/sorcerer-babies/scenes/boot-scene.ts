/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @description  Space Invaders: Boot Scene
 * @license      Digitsensitive
 */

import { AnimationHelper } from "../helpers/animation-helper";

export class BootScene extends Phaser.Scene {
  private animationHelperInstance: AnimationHelper;
  private loadingBar: Phaser.GameObjects.Graphics;
  private progressBar: Phaser.GameObjects.Graphics;

  constructor() {
    super({
      key: "BootScene"
    });
  }

  preload(): void { 
    // set the background and create loading bar
    this.cameras.main.setBackgroundColor(0x98d687);
    this.createLoadingbar();

    // pass value to change the loading bar fill
    this.load.on(
      "progress",
      function(value) {
        this.progressBar.clear();
        this.progressBar.fillStyle(0xfff6d3, 1);
        this.progressBar.fillRect(
          this.cameras.main.width / 4,
          this.cameras.main.height / 2 - 16,
          (this.cameras.main.width / 2) * value,
          16
        );
      },
      this
    );

    // delete bar graphics, when loading complete
    this.load.on(
      "complete",
      function() {
        /*this.animationHelperInstance = new AnimationHelper(
          this,
          this.cache.json.get("animationJSON")
        );*/
        this.progressBar.destroy();
        this.loadingBar.destroy();
      },
      this
    );

    // load out package
    /*this.load.pack(
      "preload",
      "./src/virgin-trains/assets/pack.json",
      "preload"
    );   */
    this.load.image('TownMap','./src/sorcerer-babies/assets/sprites/TownMap.png');    
    this.load.image('Well','./src/sorcerer-babies/assets/sprites/Well.png');     
    this.load.image('goblin','./src/sorcerer-babies/assets/sprites/GoblinScout.png');     
    this.load.image('hobgoblin','./src/sorcerer-babies/assets/sprites/Hobgoblin.png');    
    this.load.image('bugbear','./src/sorcerer-babies/assets/sprites/Bugbear.png');     
    this.load.image('sorcerer-attack','./src/sorcerer-babies/assets/sprites/SorcererAttack.png');     
    this.load.image('sorcerer-dead','./src/sorcerer-babies/assets/sprites/SorcererDead.png');      
    this.load.image('green-dragon','./src/sorcerer-babies/assets/sprites/GreenDragon.png');     
    this.load.tilemapTiledJSON('town-map','./src/sorcerer-babies/assets/worlds/town.json');  
    this.load.json("strings",'./src/sorcerer-babies/assets/strings.json')  
  }

  update(): void {
    console.log("Start game!");
    this.scene.start("GameScene");
    //this.scene.start("FailScene");
  }

  private createLoadingbar(): void {
    this.loadingBar = this.add.graphics();
    this.loadingBar.fillStyle(0x5dae47, 1);
    this.loadingBar.fillRect(
      this.cameras.main.width / 4 - 2,
      this.cameras.main.height / 2 - 18,
      this.cameras.main.width / 2 + 4,
      20
    );
    this.progressBar = this.add.graphics();
  }
}
