 

export class FailScene extends Phaser.Scene { 

  constructor() {
    super({
      key: "FailScene"
    });
  }

  init(): void { 
    // set the background and create loading bar
    this.cameras.main.setBackgroundColor(0xFF8787); 
    let text = this.add.text(6,2,"Game Over!\nThe Goblin Army has overrun the town!\nThey now control the well and the rest of the world is doomed to be overrun by Goblin Sorcerer Babies!");
    text.setOrigin(0.5,0.5);
    text.setPosition(this.cameras.main.width/2,this.cameras.main.height/2);
    text.setAlign("center");
    text.setWordWrapWidth(this.cameras.main.width-6); 
    text.addListener("pointerup",() => {
        window.location.reload();
    });
    text.setInteractive();
  } 
}
