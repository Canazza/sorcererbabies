/**
 * @author       David McQuillan
 * @copyright    2018 Clydebuilt Games
 * @description  Something
 * @license      David McQuillan
 */

/// <reference path="../phaser.d.ts"/>

import "phaser"; 
import { GameScene } from "./scenes/game-scene"; 
import { BootScene } from "./scenes/boot-scene"; 
import { FailScene } from "./scenes/fail-scene";
import { WinScene } from "./scenes/win-scene";
let scale = 3;
let desiredHeight = 768/scale;
let desiredWidth = 1024/scale

function getConfig():GameConfig { 
 return {
    title: "Sorcerer Babies",
    //url: "https://github.com/digitsensitive/phaser3-typescript",
    version: "1.0",
    width: desiredWidth,
    height: desiredHeight, 
    zoom:  scale,
    type: Phaser.AUTO,  
    parent: "game",
    scene: [BootScene,GameScene,WinScene,FailScene],
    input: {
      keyboard: true,
      mouse:true
    },
    physics: {
      default: "arcade",
      arcade: {
        gravity: { y: 500 },
        debug: false
      }
    },
    backgroundColor: "#FF0000",
    pixelArt: true,
    antialias: false 
  };
}
export class Game extends Phaser.Game {
  constructor(config: GameConfig) {
    super(config);
  }
}
let game:Game = null; 
window.onload = () => { 
  game = new Game( getConfig());  
};