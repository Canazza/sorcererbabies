import { Button } from "./button";
import { GameObjects } from "phaser";

export class MessageBox extends Phaser.GameObjects.Group {
    private _config:MessageBoxConfig = null; 
    private graphics:Phaser.GameObjects.Graphics;
    private headingText:Phaser.GameObjects.Text;
    private messageText:Phaser.GameObjects.Text; 
    public height;
    public width;
    public x;
    public y;
    private buttons:Button[];

    constructor(scene:Phaser.Scene,x=0,y=0, config:MessageBoxConfig = null) {
        super(scene); 
        this.x = x;
        this.y = y;
        this.buttons = [];
        this.graphics = new Phaser.GameObjects.Graphics(this.scene,null);
        this.headingText = new Phaser.GameObjects.Text(this.scene,2,2,"" ,{fontFamily:"Open Sans",fontSize:"16px",color:"#000"});
        this.messageText = new Phaser.GameObjects.Text(this.scene,2,18,"",{fontFamily:"Open Sans",fontSize:"12px",color:"#000"});
        
        this.add(this.graphics);
        this.add(this.headingText);
        this.add(this.messageText);
        this.graphics.setScrollFactor(0);
        this.messageText.setScrollFactor(0);
        this.headingText.setScrollFactor(0);
        this.scene.add.existing(this.graphics);
        this.scene.add.existing(this.headingText);
        this.scene.add.existing(this.messageText);
        this.config = config;   
    }
    public get config():MessageBoxConfig {
        return this._config;
    }
    public set config(config:MessageBoxConfig) {
        this._config = config;
        if(!this.config) {
            console.warn("No config passed to MessageBox");
            return;
        }
        this.headingText.setText(config.title);
        this.messageText.setText(config.message);
        console.log("Show Heading?" , !!config.title);
        this.headingText.setVisible(!!config.title);
        this.messageText.setVisible(!!config.message);

        let elementWidth = this.config.width - (this.config.padding * 2);
        this.headingText.setWordWrapWidth(elementWidth )
        this.messageText.setWordWrapWidth(elementWidth )
        this.buttons.forEach((btn) => btn.setVisible(false));
        this.config.buttons.forEach((bcfg,i) => {

            let button = this.buttons[i];
            if(!button) {
                console.log("No Button in Pool, creating new one");
                button = new Button(this.scene,0,0,bcfg.text); 
                this.buttons.push(button);
                this.add(button); 
                this.scene.add.existing(button);
            }
            button.width = elementWidth;
            button.setText(bcfg.text);  
            button.removeAllListeners("click");
            button.addListener("click", () => { 
                this.setVisible(false);
                bcfg.callback();
            });
            button.setScrollFactor(0);
            this.remove(button,false,false);
            this.add(button);
        })

        this.arrangeVertically();  
        this.setVisible(true);
    }  
    public setVisible(visible:boolean):void {
        this.getChildren().forEach((v) => v['setVisible'] ? v['setVisible'](visible):null);
        this.buttons.forEach((btn) => btn.setVisible(false));
        this.config.buttons.forEach((btncfg,i) => {
            this.buttons[i].setVisible(visible);
        })
        if(visible){
            this.arrangeVertically();
        }
    }
    private arrangeVertically() { 
        this.graphics.clear();
        this.headingText.setVisible(!!this.config.title);
        this.messageText.setVisible(!!this.config.message);
        let xPos = this.x + this.config.padding;
        let yPos = this.y + this.config.padding;
        this.getChildren().forEach((go:GameObjects.GameObject,i,arr) => {
            if(go == this.graphics) return; 
            if(go['visible'] == false) return;  
            go['x'] = xPos;
            go['y'] = yPos; 
            
            go['draw'] ? go['draw']():null;
            let bounds:Phaser.Geom.Rectangle = go['getBounds'] ? go['getBounds']():null;
            if(bounds) { 
                yPos += bounds.height;
            } else {
                yPos += go['height'] || 24;
            }
            if(i < arr.length - 1) yPos += this.config.buttonSpacing;

        }); 
        yPos += this.config.padding;
        this.height = yPos - this.y;
        this.graphics.fillStyle(0xFFFFFF);
        this.graphics.fillRect(this.x,this.y,this.config.width,this.height );
        this.graphics.lineStyle(2,0x000000);
        this.graphics.strokeRect(this.x,this.y,this.config.width,this.height ); 
    }
    public centerOn(x,y) {
        this.x = x- this.config.width / 2;
        this.y = y- this.height /2; 
        this.arrangeVertically();  
    }
} 
export class MessageBoxConfig {
    public static OK_CANCEL(title:string,message:string,ok_callback:Function, cancel_callback:Function):MessageBoxConfig { 
        return new MesssageBoxConfigFactory().setTitle(title).setMessage(message).addButton("OK",ok_callback).addButton("CANCEL",cancel_callback).Build();
    }
    public static OK(title:string,message:string,ok_callback:Function):MessageBoxConfig { 
        return new MesssageBoxConfigFactory().setTitle(title).setMessage(message).addButton("OK",ok_callback).Build();
    }
    public title:string;
    public message:string;
    public buttons:MessageBoxButtonConfig[] = [];
    public width:number = 150;
    public padding:number = 4;
    public buttonSpacing:number = 3;
}
export class MessageBoxButtonConfig { 
    public text:string;
    public callback:Function;
}
export class MesssageBoxConfigFactory {
    private config:MessageBoxConfig = new MessageBoxConfig();
    constructor() {}
    public Build():MessageBoxConfig {        
        return this.config;
    }
    public setTitle(title:string):MesssageBoxConfigFactory {
        this.config.title = title;
        return this;
    }
    public setMessage(message:string):MesssageBoxConfigFactory {
        this.config.message = message;
        return this;
    }
    public setWidth(width:number):MesssageBoxConfigFactory {
        this.config.width = width;
        return this;
    }
    public setPadding(padding:number):MesssageBoxConfigFactory {
        this.config.padding = padding;
        return this;
    }
    public addButton(text:string,callback:Function):MesssageBoxConfigFactory {
        let button = new MessageBoxButtonConfig();
        button.text = text;
        button.callback = callback;
        this.config.buttons.push(button);
        return this;
    }
}