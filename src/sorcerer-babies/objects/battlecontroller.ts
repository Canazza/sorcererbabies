import { MathHelper } from "../helpers/math-helper";
import { TownConfig } from "./gamecontroller";

export class BattleController extends Phaser.Events.EventEmitter {
    private scene:Phaser.Scene;
    constructor(scene:Phaser.Scene) {
        super();
        this.scene = scene;
    }
    public DoBattle(config:BattleConfig, town: TownConfig):BattleEvent[] {

        let eventList:BattleEvent[] = []; 
            eventList.push(new BattleEvent(BattleEvent.MESSAGE,0,"vs " + town.babyCount + " sorcerer babies",0)); 
        while(town.babyCount > 0 && config.enemies.filter((enemy) => enemy.health > 0).length > 0) {
            eventList = [... eventList, ... this.EnemyTurn(config,town), ... this.SorcererTurn(config,town)]; 
        }
        if(town.babyCount <= 0) {
            eventList.push(new BattleEvent(BattleEvent.GAMEOVER,-1));
        } else {
            eventList.push(new BattleEvent(BattleEvent.BATTLE_WON,-1));
        }

        return eventList;
    }
    private EnemyTurn(config:BattleConfig, town: TownConfig):BattleEvent[] {
        
        let eventList:BattleEvent[] = [];
        eventList.push(new BattleEvent(BattleEvent.MESSAGE,0,"Enemy's Turn",0));
        let skipAttacksFor = -1;
        config.attacks.forEach((attack) => {
            if(!attack.canUse) {
                attack.chargeCount --;
                return;
            }
            let enemy = config.enemies[attack.owner];
            if(skipAttacksFor == attack.owner) return;
            if(attack.endIfUsed) skipAttacksFor = attack.owner;
            attack.chargeCount = attack.recharge;
            if(enemy.health <= 0) {
                eventList.push(new BattleEvent(BattleEvent.BATTLE_ENEMY_MISS,attack.owner, "Is Dead"));
                return;
            }
            if(!attack.isAoE) {
                if(MathHelper.roll(attack.hitRoll) < 10) {
                    eventList.push(new BattleEvent(BattleEvent.BATTLE_ENEMY_MISS,attack.owner, attack.name + " Missed"));
                    console.log(attack.name,"MISSED");
                    return;
                }
            }
            console.log(attack.name,"HIT");
            let damage = MathHelper.roll(attack.damageRoll);
            eventList.push(new BattleEvent(BattleEvent.BATTLE_ENEMY_HIT,attack.owner,attack.name,damage));
            let damageevnts = town.dealDamage(damage,attack.targets,attack.isAoE);
            eventList = [... eventList, ... damageevnts];
        });
        return eventList;
    }
    private SorcererTurn(config:BattleConfig, town: TownConfig):BattleEvent[] {
        
        if(town.babyCount <= 0) return [];
        let eventList:BattleEvent[] = [];
        let totalDamage = town.babyAttackDamage * town.babyCount;
        
        eventList.push(new BattleEvent(BattleEvent.BATTLE_BABY_ATTACK,-1,town.babyCount + " Attack!",totalDamage));
        config.enemies.forEach((enemy,i) => {
            if(totalDamage <= 0) return;
            if(enemy.health <= 0) return;
            if(enemy.health <= totalDamage) {
                totalDamage -= enemy.health;
                eventList.push(new BattleEvent(BattleEvent.BATTLE_ENEMY_KILLED,i,"Killed",enemy.health));
                enemy.health = 0;
            } else {
                eventList.push(new BattleEvent(BattleEvent.BATTLE_ENEMY_DAMAGED,i,"Damaged",totalDamage));
                enemy.health -= totalDamage;
                totalDamage = 0;
            }
        })
        return eventList;
    }
}
export class BattleEvent{
    public static MESSAGE = "message" ;
    public static BATTLE_ENEMY_HIT = "enemyhit" ;
    public static BATTLE_ENEMY_MISS = "enemymiss"; 
    public static BATTLE_ENEMY_KILLED = "enemykilled"; 
    public static BATTLE_ENEMY_DAMAGED = "enemydamaged"; 
    public static BATTLE_BABY_ATTACK =  "babyattack" ;
    public static BATTLE_BABY_KILLED = "babykilled";
    public static GAMEOVER = "gameover";
    public static BATTLE_WON = "battlewon";
    constructor(type, index, text:string = null, damage = null) {
        this.type = type;
        this.enemyIndex = index;
        this.text = text;
        this.damage = damage;
    }
    public type:string;
    public enemyIndex:integer;
    public text:string;
    public damage:integer;
}
export class BattleConfig {
    public configName:string;
    public enemies:EnemyConfig[] = [];
    public attacks:AttackConfig[] = [];
    public get aliveEnemies():EnemyConfig[] {
        return this.enemies.filter((enemy) => enemy.health > 0);
    }
}
export class AttackConfig {
    public name:string;
    public owner: integer;
    public damageRoll:string;   
    public hitRoll:string;    
    public targets: integer;
    public isAoE:boolean = false;
    public recharge:integer = 1;
    public chargeCount:integer = 1;
    public endIfUsed:boolean = false;
    public get canUse():boolean  {
        return this.chargeCount <= 1;
    }
}
export class EnemyConfig {
    public name:string;
    public sprite:string;
    public health:integer;
}
export class BattleFactory {
    constructor(name:string) { this.config.configName = name;}
    private config:BattleConfig = new BattleConfig();
    private currentAttackConfig:AttackConfig;
    private currentEnemyConfig:EnemyConfig;
    public Build():BattleConfig {
        return this.config;
    }
    public addEnemy(name:string,sprite:string):BattleFactory { 
        let enemy = new EnemyConfig();
        enemy.name = name;
        enemy.sprite = sprite;
        this.currentEnemyConfig = enemy;
        this.config.enemies.push(enemy);
        return this;
    }
    public setHealth(hp:integer):BattleFactory {
        this.currentEnemyConfig.health = hp;
        return this;
    }
    public addAttack(name:string):BattleFactory { 
        let attack = new AttackConfig();
        attack.name = name;
        attack.owner = this.config.enemies.length - 1;
        this.currentAttackConfig = attack;
        this.config.attacks.push(attack);
        return this;
    }
    public setRoll(hitRoll:string,damageRoll:string):BattleFactory {
        this.currentAttackConfig.damageRoll = damageRoll;
        this.currentAttackConfig.hitRoll = hitRoll;
        return this;
    }
    public setAoE(isAoE:boolean) {
        this.currentAttackConfig.isAoE = isAoE;
        return this;
    }
    public setTargets(targets:integer):BattleFactory {
        this.currentAttackConfig.targets = targets;
        return this;
    }
    public setRecharge(recharge:integer):BattleFactory {
        this.currentAttackConfig.recharge = recharge;
        return this;
    }
    public setEndIfUsed(endIfUsed:boolean):BattleFactory{
        this.currentAttackConfig.endIfUsed = endIfUsed;
        return this;
    }
}