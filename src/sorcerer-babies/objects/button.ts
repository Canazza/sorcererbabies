export class Button extends Phaser.GameObjects.Container {
    private text:Phaser.GameObjects.Text;
    private graphics:Phaser.GameObjects.Graphics;
    public width:number;
    public height:number;
    public paddingX:number = 4;
    public paddingY:number = 1;
    private actualWidth:number;
    private actualHeight:number;
    constructor(scene,x = 0,y = 0,text:string = "", style:any = {fontFamily:"Open Sans",fontSize:"16px", color:"#FF0000"}) {
        super(scene,x,y);
        this.text = new Phaser.GameObjects.Text(scene,1,1,text,style);
        this.graphics = new Phaser.GameObjects.Graphics(scene,null);
        this.add(this.graphics);
        this.add(this.text);
        this.addListener('pointerdown',  this.onDown)
        .addListener('pointerup',  this.onUp)
        .addListener('pointerover',  this.onHover)
        .addListener('pointerout',  this.onLeave); 
        this.draw();
         this.text.setActive(true);

    }
    private onHover() { 
        this.text.setColor("#F80");
    }
    private onLeave() { 
        this.text.setColor("#F00");
    }
    private onDown() { 
        this.text.setColor("#FF0");
    }
    private onUp() { 
        this.text.setColor("#F00");
        this.emit("click");
    }
    public setText(txt:string) {
        this.text.setText(txt);
    }
    public draw():void {
        let height = 0;
        let width = 0;
        if(this.width <= 0) {
            width = this.text.getBounds().width + this.paddingX*2;
        } else {
            width = this.width;
            this.text.setWordWrapWidth(this.width - this.paddingX*2);
        }
        if(this.height <= 0) {
            height = this.text.getBounds().height + this.paddingY*2;
        }  else {
            height = this.height;
        }
        var textBounds = this.text.getBounds();
        this.text.setOrigin(0.5,0.5);
        this.text.x = width/2;
        this.text.y = height/2; 
        this.graphics.clear();
        this.graphics.fillStyle(0xFFFF00);
        this.graphics.lineStyle(1,0xFF8800);
        this.graphics.fillRect(0,0,width,height);
        this.graphics.strokeRect(0,0,width,height); 
        this.actualHeight=height;
        this.actualWidth=width;

        
        this.setSize(this.actualWidth,this.actualHeight);
        this.setInteractive({useHandCursor:true});
        let p = new Phaser.Geom.Point(this.x,this.y); 
        let np:any = this.getWorldTransformMatrix().transformPoint(this.x,this.y,p); 
        this.input.hitArea.x = this.actualWidth/2;//=  this.actualWidth/2;
        this.input.hitArea.y = this.actualHeight/2;
        this.input.hitArea.width = this.actualWidth;//=  this.actualWidth/2;
        this.input.hitArea.height =  this.actualHeight;
        //this.graphics.fillStyle(0xFF00FF);
        //this.graphics.fillRect(this.input.hitArea.x,this.input.hitArea.y,this.input.hitArea.width,this.input.hitArea.height); 

    }
}