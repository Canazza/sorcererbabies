import { MessageBox, MesssageBoxConfigFactory, MessageBoxConfig } from "./messagebox";
import { l18n } from "../helpers/l18n";

export class Intro extends Phaser.Events.EventEmitter {
    public static COMPLETED_EVENT:string = "completed";
    private popup:MessageBox;
    private scene:Phaser.Scene;
    public intro1:MessageBoxConfig[] = [
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_1").addButton("INTRO_BUTTON_1", () => { this.nextPopup(); }).addButton("(Skip Intro)", () => { this.showPopup(-1); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_2").addButton("INTRO_BUTTON_2", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_3").addButton("INTRO_BUTTON_3", () => { this.nextPopup(); }).Build(),

        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_4").addButton("INTRO_BUTTON_4", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_5").addButton("INTRO_BUTTON_5", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_6").addButton("INTRO_BUTTON_6", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_6b").addButton("OK", () => { this.nextPopup(); }).Build(),

        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_7").addButton("INTRO_BUTTON_7", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_8").
        addButton("INTRO_BUTTON_8", () => { this.showPopup(10); }).
        addButton("INTRO_BUTTON_8b", () => { this.showPopup(9); }).
        Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_9a").addButton("INTRO_BUTTON_9a", () => { this.showPopup(11); }).Build(),

        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_9b").addButton("INTRO_BUTTON_9b", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_10").addButton("INTRO_BUTTON_10", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_11").addButton("INTRO_BUTTON_11", () => { this.nextPopup(); }).Build(), 
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_12").addButton("INTRO_BUTTON_12", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_13").addButton("INTRO_BUTTON_13", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_14").addButton("INTRO_BUTTON_14", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_15").addButton("INTRO_BUTTON_15", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_16").addButton("INTRO_BUTTON_16", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_17").addButton("INTRO_BUTTON_17", () => { this.nextPopup(); }).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_18").addButton("INTRO_BUTTON_18", () => { this.nextPopup(); }).setWidth(250).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_19").addButton("INTRO_BUTTON_19", () => { this.nextPopup(); }).setWidth(250).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO_TEXT_20").addButton("INTRO_BUTTON_20", () => { this.nextPopup(); }).Build()
    ]
    public intro2:MessageBoxConfig[] = [
        new MesssageBoxConfigFactory().setMessage("INTRO2_TEXT_1").addButton("INTRO2_BUTTON_1", () => { this.nextPopup(); }).setWidth(250).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO2_TEXT_2").addButton("INTRO2_BUTTON_2", () => { this.nextPopup(); }).setWidth(300).Build(),
        new MesssageBoxConfigFactory().setMessage("INTRO2_TEXT_3").addButton("INTRO2_BUTTON_3", () => { this.nextPopup(); }).setWidth(300).Build(),

        new MesssageBoxConfigFactory().setMessage("INTRO2_TEXT_4").addButton("INTRO2_BUTTON_4", () => { this.nextPopup(); }).setWidth(250).Build()
    ]
    constructor(scene:Phaser.Scene) {
        super();
        this.scene = scene;
        this.popup = new MessageBox(this.scene,0,0,null);
    }
    start():void {
        
    }  
    private popupDetails:MessageBoxConfig[];
    setConfig(configList:MessageBoxConfig[]) {
        this.popupDetails = configList;
        this.showPopup(0);
    }
    private currentIndex = 0;
    nextPopup() {
        this.showPopup(this.currentIndex + 1);
    }
    showPopup(index) { 
        
        if(index >= this.popupDetails.length || index < 0) { 
            this.emit(Intro.COMPLETED_EVENT);
            return;
        }
        this.currentIndex = index;
        let config = this.popupDetails[index];
        config.title = l18n.get(config.title);
        config.message = l18n.get(config.message);
        config.buttons.forEach((b)=> { b.text = l18n.get(b.text)});
        this.popup.config = config;
        this.popup.width = 200; 
        this.popup.centerOn(this.scene.cameras.main.width/2,this.scene.cameras.main.height/2);

    }
}