import { BattleEvent, BattleConfig } from "./battlecontroller";
import { MathHelper } from "../helpers/math-helper";

export class BattleEventDisplay extends Phaser.GameObjects.Container{
    //private sprite:Phaser.GameObjects.Sprite; 
    private enemySprites:Phaser.GameObjects.Sprite[];
    private border:Phaser.GameObjects.Graphics;
    private attacker:Phaser.GameObjects.Text;
    private text:Phaser.GameObjects.Text;
    public width:number = 200;
    public height:number = 200;
    public padding:number = 2;
    private nextButton:Phaser.GameObjects.Graphics;
    constructor(scene:Phaser.Scene) {
        super(scene);
        //this.sprite = new Phaser.GameObjects.Sprite(scene,this.padding,this.padding,""); 
        this.border = new Phaser.GameObjects.Graphics(scene,null);
        this.border.fillStyle(0xFFFFFF);
        this.border.lineStyle(2,0x000000);
        this.border.fillRect(0,0,this.width,this.height);
        this.border.strokeRect(0,0,this.width,this.height);
        this.enemySprites = [];
        this.add(this.border);
        //this.add(this.sprite);
        this.text = new Phaser.GameObjects.Text(scene,0,0,"Info",{fontFamily:"Courier", fontSize:"14px",color:"#000"});
        this.text.setOrigin(0.5,0);
        this.text.setAlign("center");
        this.add(this.text);
        this.attacker = new Phaser.GameObjects.Text(scene,0,0,"Info",{fontFamily:"Courier", fontSize:"16px",color:"#000"});
        this.attacker.setOrigin(0.5,0);
        this.add(this.attacker); 
        this.nextButton = new Phaser.GameObjects.Graphics(scene,null);
        this.scene.add.existing(this.nextButton);  
        this.nextButton.setScrollFactor(0);
    }
    private displaySpriteList(textures:string[]) {
        let toShow = Math.min(20,textures.length);
        let displayWidth = toShow * 32;
        let minX = (this.width - displayWidth)/2;
        let maxX = minX + displayWidth;
        if(displayWidth > this.width - this.padding * 2 - 32) {
            displayWidth = this.width - this.padding*2 - 16;
            minX = (this.width - displayWidth)/2 - 8;
            maxX = minX + displayWidth;
        }
        console.log("displaySpriteList",textures,toShow,displayWidth,minX,maxX) 
        this.enemySprites.forEach((spr) => spr.setVisible(false));
        for(let i = 0; i < toShow ; i++) {
            let sprite = this.enemySprites[i];
            if(!sprite) {
                sprite = new Phaser.GameObjects.Sprite(this.scene,0,0,textures[i]);
                this.enemySprites.push(sprite);    
                this.add(sprite);            
            }
            sprite.setTexture(textures[i]);
            sprite.setPosition(MathHelper.lerp(minX,maxX,i/toShow) + 16,this.padding + 16);
            sprite.setVisible(true);
        }
    }
    public displayEvent(battleConfig:BattleConfig, battleEvent:BattleEvent) {
        console.log("displaying event",battleEvent);
        this.timeOutDuration  = 5000;

        switch(battleEvent.type) {
            case BattleEvent.MESSAGE: 
                if(battleEvent.enemyIndex >= 0) { 
                    this.attacker.setText(battleConfig.configName);
                    //this.sprite.setTexture((battleConfig.aliveEnemies[0] || battleConfig.enemies[0]).sprite);
                    this.displaySpriteList(battleConfig.enemies.map((enemy) => enemy.sprite));
                } else {                    
                    this.attacker.setText("Babies");
                    //this.sprite.setTexture("sorcerer-attack");
                    this.displaySpriteList(["sorcerer-attack"]);
                }
                this.text.setText(battleEvent.text);
                this.timeOutDuration  = 7500;
                break;
            case BattleEvent.BATTLE_BABY_ATTACK:
                this.attacker.setText("Babies");
                //this.sprite.setTexture("sorcerer-attack");
                this.displaySpriteList(["sorcerer-attack"]);
                this.text.setText(battleEvent.text);
                this.timeOutDuration  = 7500;
                break;
            case BattleEvent.BATTLE_BABY_KILLED:
                //this.sprite.setTexture("sorcerer-dead");
                this.displaySpriteList(["sorcerer-dead"]);
                this.text.setText(battleEvent.text);
                this.timeOutDuration  = 2500;
            break;
            case BattleEvent.BATTLE_ENEMY_HIT:
                this.attacker.setText(battleConfig.enemies[battleEvent.enemyIndex].name);
                //this.sprite.setTexture(battleConfig.enemies[battleEvent.enemyIndex].sprite);
                this.displaySpriteList([battleConfig.enemies[battleEvent.enemyIndex].sprite]);
                this.text.setText(battleEvent.text + "\n"+battleEvent.damage +" damage");
            break;
            case BattleEvent.BATTLE_ENEMY_MISS:
                this.attacker.setText(battleConfig.enemies[battleEvent.enemyIndex].name);
                //this.sprite.setTexture(battleConfig.enemies[battleEvent.enemyIndex].sprite);
                this.displaySpriteList([battleConfig.enemies[battleEvent.enemyIndex].sprite]);
                this.text.setText(battleEvent.text);
                break;
            case BattleEvent.BATTLE_ENEMY_KILLED:
                this.attacker.setText(battleConfig.enemies[battleEvent.enemyIndex].name);
                //this.sprite.setTexture(battleConfig.enemies[battleEvent.enemyIndex].sprite);
                this.displaySpriteList([battleConfig.enemies[battleEvent.enemyIndex].sprite]);
                this.text.setText(battleEvent.text + "\nTook "+battleEvent.damage +" damage");
                break;
            case BattleEvent.BATTLE_ENEMY_DAMAGED:
                this.attacker.setText(battleConfig.enemies[battleEvent.enemyIndex].name);
                //this.sprite.setTexture(battleConfig.enemies[battleEvent.enemyIndex].sprite);
                this.displaySpriteList([battleConfig.enemies[battleEvent.enemyIndex].sprite]);
                this.text.setText(battleEvent.text + "\nTook "+battleEvent.damage +" damage");
                break;
            case BattleEvent.BATTLE_WON:
                this.attacker.setText(battleConfig.configName);
                this.displaySpriteList(["sorcerer-attack"]);
                this.text.setText("The attack has been repelled!");
                this.timeOutDuration  = 7500;
                break;
            case BattleEvent.GAMEOVER:
                this.attacker.setText("Game Over");
                this.displaySpriteList(["sorcerer-dead"]);
                this.text.setText("The town has been overrun!");
                this.timeOutDuration  = 15000;
                break;
        }
        this.border.clear();        
        this.border.fillStyle(0xFFFFFF);
        this.border.lineStyle(2,0x000000);
        this.border.fillRect(0,0,this.width,this.height);
        this.border.strokeRect(0,0,this.width,this.height);
        //this.sprite.setPosition(this.width  / 2,this.padding + this.sprite.height / 2);
        this.attacker.setPosition(this.width  / 2,32);
        this.text.setPosition(this.attacker.x, this.attacker.y + this.attacker.getBounds().height + 8);
        this.text.setWordWrapWidth(this.width - this.padding * 2);
        this.attacker.setWordWrapWidth(this.width - this.padding * 2); 
        this.nextButton.removeAllListeners("pointerup");
        this.nextButton.setDepth(10); 
        this.nextButton.clear();
        this.nextButton.fillStyle(0x222222);
        this.nextButton.fillCircle(0,0,16);
        this.setSize(this.width,this.height);
        this.setInteractive({useHandCursor:true});
        this.input.hitArea.x = this.width/2;
        this.input.hitArea.y = this.height/2;
        this.addListener("pointerup", () => {     
            if(!this.timeoutEvent) return;
            this.timeoutEvent.paused = true;
            this.timeoutEvent.callback();
        })
        this.timeoutEvent = this.scene.time.addEvent({ delay: this.timeOutDuration, callback: () => {
            this.emit("completed",battleEvent);   
            this.timeoutEvent = null;
        }, callbackScope: this});  
    }
    public setVisible(isVisible:boolean) {
        super.setVisible(isVisible);
        this.nextButton.setVisible(isVisible);
        return this;
    }
    private timeoutEvent:Phaser.Time.TimerEvent;
    private timeOutDuration:number; 
    public centerOn(x,y) {
        this.x = x- this.width / 2;
        this.y = y- this.height /2;  
    }
    public update() {
        if(!!this.timeoutEvent) {
            let t  = this.timeoutEvent.elapsed / this.timeoutEvent.delay; 
            this.nextButton.setPosition(this.x + this.width - 24, this.y + this.height - 24);
            this.nextButton.clear();
            this.nextButton.fillStyle(0x222222);
            this.nextButton.fillCircle(0,0,16);
            
            this.nextButton.lineStyle(2,0x000000);

            this.nextButton.strokeCircle(0,0,16);

            this.nextButton.lineStyle(2,0xFF8800);
            this.nextButton.fillStyle(0xFF8800);

            this.nextButton.beginPath();
            this.nextButton.arc(0,0,16,0,t * Math.PI * 2);
            this.nextButton.strokePath();
            this.nextButton.fillTriangle(-4,-8,6,0,-4,8);
            this.nextButton.setInteractive(); 
        }
    }
}