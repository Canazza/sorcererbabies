import { BattleEvent } from "./battlecontroller";
import { MathHelper } from "../helpers/math-helper";

export class GameController {
    public town: TownConfig = new TownConfig();
    public capacityToAdd: integer = 0;
    public recruitment:integer = 0;
    public builders:integer = 0;
    public gatherers:integer = 0;
    public get gold():integer {
        return this.storedResources - this.usedResources;
    }
    public storedResources:integer = 10;
    public usedResources:integer = 0;
    public get  populationAtWork():integer {
        return this.recruitment + this.builders + this.gatherers;
    }
    public get freePopulation():integer {
        return this.town.townPopulation - this.populationAtWork;
    }
    constructor() {

    }
    public ResetDay() {
        this.recruitment = 0;
        this.builders = 0;
        this.capacityToAdd = 0;
        this.gatherers = 0;
        this.usedResources = 0;
    }
    public SpawnTick():string[] { 
        let events:string[] = [];

        let oldPop = this.town.townPopulation;
        let oldFreePop = this.freePopulation;
        let oldBabyCount=  this.town.babyCount;
        let oldCapacity = this.town.townCapacity; 
        let oldResources = this.gold;

        let recruitRoll = "d6";
        console.log("Recruitment:",recruitRoll);
        let recruited = 0;
        for(let n= 0; n < this.recruitment; n++){ recruited += ((MathHelper.roll(recruitRoll) >= 6) ? 1:0);}
        console.log("Recruited",recruited);
        
        let gathered =  MathHelper.roll((this.gatherers*2) + "d4");
        this.storedResources -= this.usedResources;
        this.usedResources = 0;
        this.storedResources += gathered;
        this.town.crecheCount += this.capacityToAdd;

        this.town.babyCount = MathHelper.clamp(this.town.babyCount + oldFreePop,0,this.town.townCapacity);

        this.town.townPopulation += recruited; 

        events.push(oldFreePop + " were not given a job and are assigned to birthing Sorcerer Babies");
        events.push("Town population grew from "+ oldPop + " to " + this.town.townPopulation);
        events.push("Baby Capacity increased from " + oldCapacity + " to " + this.town.townCapacity);
        events.push("Baby population grew from "+ oldBabyCount + " to " + this.town.babyCount);
        events.push("Building Resources changed from "+ oldResources + " to " + this.gold);
        
        this.recruitment = 0;
        this.builders = 0;
        this.capacityToAdd = 0;
        this.gatherers = 0;

        return events;
    }
    public canRecruit(count:integer):boolean {
        return (this.populationAtWork + count*2) <= this.town.townPopulation
    }
    public canBuildCreche(count:integer):boolean {
        return (this.gold >= 10*count) && (this.populationAtWork + 2 * count) <= this.town.townPopulation;
    }
    public canGatherResources(count:integer):boolean {
        return (this.populationAtWork + 2 * count) <= this.town.townPopulation;
    }
    public buildCreche(count:integer) {
        this.builders += 2 * count;
        this.capacityToAdd += count;
        this.usedResources += 10* count;
    }
    public recruitPeople(count:integer) {

        this.recruitment += 2 * count;
        this.recruitment = MathHelper.clamp(this.recruitment,0,this.town.townPopulation);
    }
    
    public gatherResources(count:integer) {
        this.gatherers += count * 2;
    }
}
export class TownConfig {
    public crecheCount:integer = 0;
    public get townCapacity():integer  {
        return (this.crecheCount * 2) + Math.floor(this.townPopulation / 2);
    }
    public babyCount:integer = 10;
    public townPopulation:integer = 20;
    public babyHealth:integer = 4;
    public babyAttackDamage:integer = 2; 
    
    public dealDamage(damage:integer, maxTargets:integer, isAoE:boolean):BattleEvent[] { 
        let events:BattleEvent[] = [];
        let babiesKilled = 0;
        let damageRemaining = damage;
        
        for(let i:integer = 0; i < Math.min(this.babyCount, maxTargets); i++) {
            if(damageRemaining >= this.babyHealth) {  
                babiesKilled ++;                
            }  
            if(!isAoE) damageRemaining -= this.babyHealth;
            
            if(damageRemaining <= 0) break;
        }
        console.log(damage,"damage on", maxTargets,"babies, Killed", babiesKilled);
        if(babiesKilled > 1) {
            events.push(new BattleEvent(BattleEvent.BATTLE_BABY_KILLED,-1,babiesKilled + " sorcerers died\n"+(this.babyCount - babiesKilled)+" left" ));
        } else if(babiesKilled > 0) {
            events.push(new BattleEvent(BattleEvent.BATTLE_BABY_KILLED,-1,babiesKilled + " sorcerer died\n"+(this.babyCount - babiesKilled)+" left" ));
        }
        this.babyCount -= babiesKilled; 
        return events;
    }
    public toString():string {
        return [this.townCapacity,this.babyCount,this.townPopulation,this.babyHealth].join(", ")
    }
}